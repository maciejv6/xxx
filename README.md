wall_follower {#wall_follower-design}
===========

To jest dokument projektowy do pakietu `wall_follower`.


# Purpose / Use cases
<!-- Required -->
<!-- Things to consider:
    - Why did we implement this feature? -->
W naszym programie zaimplementowaliśmy funkcjonalność odpowiedzialną za autonomiczne sterowanie samochodem, wykorzystując regulator PID do wyznaczania kąta skrętu kół. Powodem wyboru takiego regulatora był poprawny tor jazdy samochodu w symulatorze. 

# Design
<!-- Required -->
<!-- Things to consider:
    - How does it work? -->
Na podstawie wybranego zakresu punktów skanera laserowego z lewej i prawej strony pojazdu obliczana jest średnia odległość pojazdu od lewej i prawej strony toru. Dla próbek z danego zakresu określono limit odległości. Dla większej wartości odległości przyjmowany jest określony limit. Błąd obliczany jest jako różnica wartości odległości prawej i lewej strony.

W tym programie sterowanie kątem kół pojazdu odbywa się za pomocą regulatora PID, który dąży do utrzymania pojazdu w równej odległości od lewej i prawej strony. Prędkość pojazdu uzależniona jest od kątu skrętu kół. Wartość prędkości jest zmniejszana o bezwzględną wartość kąta skrętu przemnożoną przez współczynnik korekcyjny. 

## Assumptions / Known limits
<!-- Required -->

W pliku yaml zostały zdefiniowane parametry regulatora PID, ograniczenia przyspieszenia i kątu skrętu kół dla pojazdu.
Zmienne wcześniej zadeklarowane, które stanowią ograniczenia: 
- min_acc: 0.8 - minimalne przyspieszenie	
- max_acc: 1.5 - maksymalne przyspieszenie
- min_angle: -0.5 - maksymalny kąt skrętu w jedną stronę
- max_angle: 0.5 - maksymalny kąt skrętu drugą stronę

## Inputs / Outputs / API
<!-- Required -->
<!-- Things to consider:
    - How do you use the package / API? →-->

Dane wejściowe dla kodu zadeklarowane w pliku yaml: 
Wartości regulatora PID (wyznaczone metodą eksperymentalną): 
- kp: 1.2 - wartość członu proporcjonalnego
- ki: 0.6 - wartość członu całkującego
- kd: 0.8 - wartość członu różniczkującego

Wykorzystywany jest również czas między kolejnymi iteracjami wykonywanego programu, błąd poprzedniej iteracji.

Danymi wyjściowymi są kąt skrętu kół oraz prędkość.
Ponadto w konsoli wyświetlane są wartości:
- zadanej prędkości (v)
- kąta skrętu kół (angle_test_message)
- członu proporcjonalnego PID (pout_test_message)
- członu całkującego PID (iout_test_message)
- członu różniczkującego PID (dout_test_message)
- bierzącego błędu odległości (l+p_test_message)
- czasu jednej iteracji programu (time_test_message)

## Inner-workings / Algorithms
<!-- If applicable -->


## Error detection and handling
<!-- Required →-->
Do błędów obsługiwanych przez nasz program należy np. ustawianie odpowiednich wartości po przekroczeniu ich maksymalnej wartości (na maksymalną wartość) i po osiągnięciu wartości niższej niż wartość minimalna (na minimalną wartość).
Na poziomie budowania kodu zaimplementowano obsługę strumienia cout w celu detekcji i eliminacji błędów. Pozwoliło to na likwidację błędów popełnionych na etapie obliczeń oraz pozbycie się wartości spoza zakresu. 


# Security considerations
<!-- Required -->
<!-- Things to consider:
- Spoofing (How do you check for and handle fake input?)
- Tampering (How do you check for and handle tampered input?)
- Repudiation (How are you affected by the actions of external actors?).
- Information Disclosure (Can data leak?).
- Denial of Service (How do you handle spamming?).
- Elevation of Privilege (Do you need to change permission levels during execution?) →-->

Rozwiazaniem przyjętym w programie jest uśrednienie wartości wielu punktów otrzymanych z czujnika laserowego. Wartości przekraczające lub bliskie wartości maksymalnej i minimalnej są zastępowane wartością limitu.

Również dla wartości wyjściowych sterujących pojazdem sprawdzane są i poprawiane parametry dla określonego przedziału.

# References / External links
<!-- Optional -->


# Future extensions / Unimplemented parts
<!-- Optional -->
Zmiana zakresu punktów z czujnika laserowego uzależniona od prędkości pojazdu lub kąta skrętu kół.

# Related issues
<!-- Required -->

Problematyka opiera się na sterowaniu wybraną wartością (najczęściej utrzymywaniu jej na stałym zadanym poziomie) za pomocą regulatora PID. Podobne rozwiązania można spotkać nawet w życiu codziennym, np. utrzymywanie temperatury w domu, czy określonego natężenia oświetlenia. Ogólnie regulator PID może bezpośrednio sterować także innymi wartościami, np.: ciśnieniem, składem chemicznym, siłą, prędkością. 
